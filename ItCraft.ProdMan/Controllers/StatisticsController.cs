﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ItCraft.ProdMan.Application.Dto;
using ItCraft.ProdMan.Application.Interfaces;
using Microsoft.AspNet.Identity;
using System.Linq.Dynamic;
namespace ItCraft.ProdMan.Controllers
{
    [Authorize]
    public class StatisticsController : Controller
    {

        private readonly IProductService _productService;
        private readonly IStatisticsService _statsService;

        public StatisticsController(IProductService productProductService, IStatisticsService statsService)
        {
            _productService = productProductService;
            _statsService = statsService;
        }
        public int ManageProduct(StatisticDto statistic)
        {
            if (ModelState.IsValid)
            {
                statistic.UserId = User.Identity.GetUserId();
                _statsService.AddOrUpdate(statistic);
                return 1;
            }
            return 0;
        }
        
        public JsonResult StatisticsSummary(int productId)
        {
            var product = _productService.GetById(productId);
            var Amount = _statsService.TotalProductAmmount(productId);
            var TotalPrice = Amount * product.Price;
            return Json(new { ProductName = product.Name, Amount = Amount, Price = product.Price, TotalPrice = TotalPrice }, JsonRequestBehavior.AllowGet);
        }
       
        public JsonResult Statistics(int productId, string sidx, int rows = 10, int page = 1, string sord = "desc")
        {
            if (String.IsNullOrWhiteSpace(sidx))
            {
                sidx = "Date";
            }
            var stats = _statsService.Get(s => s.ProductId == productId).ToList();
            var root = stats.OrderBy(sidx + " " + sord).Skip(rows * (page - 1)).Take(rows);
            var records = stats.Count();
            return Json(new { root = root, page = page, total = stats.Count(), records = records }, JsonRequestBehavior.AllowGet);
        }
    }
}