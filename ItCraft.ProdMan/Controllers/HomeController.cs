﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ItCraft.ProdMan.Application;
using ItCraft.ProdMan.Application.Interfaces;
using ItCraft.ProdMan.Domain;
using ItCraft.ProdMan.Infrastracture.Repository;
using Newtonsoft.Json;
using System.Linq.Dynamic;
using System.Threading;
using ItCraft.ProdMan.Application.Dto;
using ItCraft.ProdMan.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ItCraft.ProdMan.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
  
}