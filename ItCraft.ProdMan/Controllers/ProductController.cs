﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ItCraft.ProdMan.Application.Dto;
using ItCraft.ProdMan.Application.Interfaces;
using Microsoft.AspNet.Identity;

namespace ItCraft.ProdMan.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        public ProductController(IProductService productProductService)
        {
            _productService = productProductService;

        }
        public JsonResult Products(string sidx, int rows = 10, int page = 1, string sord = "desc")
        {

            if (String.IsNullOrWhiteSpace(sidx))
            {
                sidx = "Id";
            }
            var records = _productService.Get().Count();
            var root = _productService.Get(sidx, rows, page, sord);
            return Json(new { root = root, page = page, total = root.Count(), records = records }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Delete(int productId)
        {
           var product= _productService.GetById(productId);
            if (product !=null)
          _productService.Remove(product);
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddProduct(ProductDto product)
        {
            if (ModelState.IsValid)
            {
                _productService.AddOrUpdate(product);
                return Json(new { result = "success" ,msg= "Product has been added"} , JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = "fail", errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage))}, JsonRequestBehavior.AllowGet);
        }
        public JsonResult EditProduct(ProductDto product)
        {
            if (ModelState.IsValid)
            {
                _productService.AddOrUpdate(product);
                return Json(new { result = "success", msg = "Product has been updated" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = "fail", errors = ModelState.Values.SelectMany(v => v.Errors.Select(x => x.ErrorMessage)) }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CheckUniqueProductByName(string ProductName)
        {
            return Json(!(_productService.Get(p => p.Name == ProductName).Any()), JsonRequestBehavior.AllowGet);
        }
       
    }
}