﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using ItCraft.ProdMan.Domain;
using ItCraft.ProdMan.Infrastracture.Context;
using ItCraft.ProdMan.Infrastracture.Enums;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ItCraft.ProdMan.Infrastracture.Seed
{
    public class Configuration : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        public Configuration()
        {

        }
        protected override void Seed(ApplicationDbContext context)
        {
            if (!(context.Users.Any(u => u.UserName == "test@itcraft.com.ua")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser { UserName = "test@itcraft.com.ua", PhoneNumber = "1234567678" };
                userManager.Create(userToInsert, "Password@123");
            }
            List<Product> products = new List<Product>();
            for (int i = 0; i < 1000; i++)
            {
                Product prod = new Product()
                {
                    Name = "Product"+i,
                    Price = 10+i*2
                };
                products.Add(prod);
            }
            Operation add = new Operation()
            {
                Id = (int)OperationTypes.Add,
                Type = OperationTypes.Add.ToString()
            };
            Operation remove = new Operation()
            {
                Id = (int)OperationTypes.Remove,
                Type = OperationTypes.Remove.ToString()
            };
            context.Products.AddRange(products);
            context.Operations.Add(add);
            context.Operations.Add(remove);
            context.SaveChanges();
        }
    }
}