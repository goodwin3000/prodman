using System.Data.Entity;
using ItCraft.ProdMan.Domain;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ItCraft.ProdMan.Infrastracture.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        public DbSet<Statistic> Statistics { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Operation> Operations { get; set; }
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}