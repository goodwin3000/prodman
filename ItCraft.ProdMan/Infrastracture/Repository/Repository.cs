﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using ItCraft.ProdMan.Domain;
using ItCraft.ProdMan.Domain.Interfaces;
using ItCraft.ProdMan.Infrastracture.Context;

namespace ItCraft.ProdMan.Infrastracture.Repository
{
    public class Repository<T> : IRepository<T> where T:BaseEntity 
    {
        private readonly ApplicationDbContext _context;
        public Repository()
        {
            _context = new ApplicationDbContext();

        }
        public void Add(T obj)
        {
            _context.Set<T>().Add(obj);

        }

        public T GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public IQueryable<T> GetAll()
        {
            return _context.Set<T>();
        }

      public  IQueryable<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(predicate);
        }

        public void Update(T obj)
        {
            _context.Entry(obj).State = EntityState.Modified;
        }

        public void Remove(T obj)
        {
            var entity = GetById(obj.Id);
            _context.Set<T>().Remove(entity);

        }

        public void AddOrUpdate(T obj)
        {
            var id = obj.Id;
            if (_context.Set<T>().Any(e => e.Id.CompareTo(id) == 0))
            {
                _context.Entry(obj).State = EntityState.Modified;
            }
            else
            {
                _context.Set<T>().Add(obj);
            }


        }
        public IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(predicate);
        }

        public int ExecuteQuery(string query)
        {
            return _context.Database.ExecuteSqlCommand(query);
        }

        public void Save()
        {
            _context.SaveChanges();
        }

      
    }
}