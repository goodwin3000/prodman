﻿using System.Web;
using System.Web.Mvc;

namespace ItCraft.ProdMan
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
