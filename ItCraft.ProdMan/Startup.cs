﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ItCraft.ProdMan.Startup))]
namespace ItCraft.ProdMan
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
