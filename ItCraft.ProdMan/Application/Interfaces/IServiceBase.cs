﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using ItCraft.ProdMan.Domain;

namespace ItCraft.ProdMan.Application.Interfaces
{
    public interface IServiceBase<T, TDto> where T:class 
    {
        void Add(TDto obj);
        TDto GetById(int id);
        IEnumerable<TDto> Get();
        IEnumerable<TDto> Get(Expression<Func<T, bool>> predicate);
        IEnumerable<TDto> Get(string sidx, int rows, int page, string sord); 
        void AddOrUpdate(TDto obj);
        void Update(TDto obj);
        void Remove(TDto obj);
        void Save();
    }
}