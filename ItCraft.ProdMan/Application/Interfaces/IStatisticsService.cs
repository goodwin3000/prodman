﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ItCraft.ProdMan.Application.Dto;
using ItCraft.ProdMan.Domain;

namespace ItCraft.ProdMan.Application.Interfaces
{
    public interface IStatisticsService:IServiceBase<Statistic, StatisticDto>
    {
        int TotalProductAmmount(int productId);
    }
}