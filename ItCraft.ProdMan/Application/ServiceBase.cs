﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Linq.Dynamic;
using System.Web;
using AutoMapper;
using ItCraft.ProdMan.Application.Interfaces;
using ItCraft.ProdMan.Domain;
using ItCraft.ProdMan.Domain.Interfaces;

namespace ItCraft.ProdMan.Application
{
    public class ServiceBase<T,TDto>:IServiceBase<T,TDto>  where T : class 
    {
        protected readonly IRepository<T> _repository;

        public ServiceBase(IRepository<T> repository)
        {
            _repository = repository;
        }

        public void Add(TDto obj)
        {
            var entity = Mapper.Map<T>(obj);
            _repository.Add(entity);
            Save();
        }

        public TDto GetById(int id)
        {
            return Mapper.Map <TDto> (_repository.GetById(id));
        }

        public IEnumerable<TDto> Get()
        {
            var list = _repository.GetAll();
            return Mapper.Map<IQueryable<T>, IEnumerable<TDto>> (list.AsQueryable())  ;
        }

        public IEnumerable<TDto> Get(Expression<Func<T, bool>> predicate)
        {
            var list = _repository.GetAll(predicate);
            return Mapper.Map<IQueryable<T>, IEnumerable<TDto>>(list.AsQueryable());
        }

        public IEnumerable<TDto> Get(string sidx, int rows, int page, string sord)
        {
            var list= _repository.GetAll().AsQueryable().OrderBy(sidx + " " + sord).Skip(rows * (page - 1)).Take(rows).ToList();
            return Mapper.Map<IQueryable<T>, IEnumerable<TDto>>(list.AsQueryable());
        }

        public void AddOrUpdate(TDto obj)
        {
            var entity = Mapper.Map<T>(obj);
            _repository.AddOrUpdate(entity);
            Save();
        }

        public void Update(TDto obj)
        {
            var entity = Mapper.Map<T>(obj);
            _repository.Update(entity);
            Save();
        }

        public void Remove(TDto obj)
        {
            var entity = Mapper.Map<T>(obj);
            _repository.Remove(entity);
            Save();
        }

       
        public void Save()
        {
            _repository.Save();
        }
    }
}