﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ItCraft.ProdMan.Application.Dto;
using ItCraft.ProdMan.Application.Interfaces;
using ItCraft.ProdMan.Domain;
using ItCraft.ProdMan.Domain.Interfaces;

namespace ItCraft.ProdMan.Application
{
    public class StatisticsService: ServiceBase<Statistic, StatisticDto>,IStatisticsService
    {
        public StatisticsService(IRepository<Statistic> repository):base(repository)
        {

        }

        public int TotalProductAmmount(int productId)
        {

            if (_repository.GetAll(s => s.ProductId == productId).Any())
                return _repository.GetAll(s => s.ProductId == productId).Sum(p=>p.Amount);
            return 0;
        }
    }
}