﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ItCraft.ProdMan.Application.Dto;
using ItCraft.ProdMan.Application.Interfaces;
using ItCraft.ProdMan.Domain;
using ItCraft.ProdMan.Domain.Interfaces;

namespace ItCraft.ProdMan.Application
{
    public class ProductService:ServiceBase<Product,ProductDto>,IProductService
    {
        public ProductService(IRepository<Product> repository ):base(repository)
        {
            
        }
    }
}