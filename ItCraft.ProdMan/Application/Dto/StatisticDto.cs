﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ItCraft.ProdMan.Domain;

namespace ItCraft.ProdMan.Application.Dto
{
    public class StatisticDto
    {
        [Range(0,1000)]
        [Required]
        public int Amount { get; set; }
        public int ProductId { get; set; }
        public int TypeId { get; set; }
        public string UserId { get; set; }
        public string OperationTypeName { get; set; }
        public string UserName { get; set; }
        public DateTime Date { get; set; }
    }
}