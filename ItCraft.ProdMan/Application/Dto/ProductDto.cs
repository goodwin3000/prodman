﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ItCraft.ProdMan.Application.Dto
{
    public class ProductDto
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [Range(typeof(decimal),"0.00","10000")]
        public decimal Price { get; set; }
    }
}