﻿$(function () {
    var editProductId;
    var dialogManageProduct;
    var dialogAddProduct;
    var dialogDelProduct;
    //-----------setup-----------------------
    function viewState() {
        $("#EditProduct").attr("disabled", false);
        $("#SaveProduct,#CancelEditProduct").attr("disabled", true);
    }
    function editState() {
        $("#EditProduct").attr("disabled", true);
        $("#SaveProduct,#CancelEditProduct").attr("disabled", false);
    }
    //-------------Dialogs------------------------------
    dialogDelProduct = $("#dialogDelProduct").dialog({
        autoOpen: false,
        height: 200,
        width: 450,
        modal: true,
        buttons: {
            "Delete": function () {
                post('/Product/Delete/', { productId: $('#dialogManageProduct').data('product').Id },
                    function () {
                        $('#list2').trigger('reloadGrid');
                        dialogDelProduct.dialog("close");
                    }
                    , null, false);
            },
            Cancel: function () {
                dialogDelProduct.dialog("close");
            }
        },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
          
            $('#productToDelete').html($('#dialogManageProduct').data('product').Name);
        },
        close: function () {
        }
    });
    dialogManageProduct = $("#dialogManageProduct").dialog({
        autoOpen: false,
        height: 500,
        width: 450,
        modal: true,
        buttons: {
            "Save": manageProduct,
            Cancel: function () {
                dialogManageProduct.dialog("close");
            }
        },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
            var product = $('#dialogManageProduct').data('product');
            $('#ManageProductName').text(product.Name);
            $('#ManageProductId').val(product.Id);
        },
        close: function () {
        }
    });
    dialogAddProduct = $("#dialogAddProduct").dialog({
        autoOpen: false,
        height: 500,
        width: 450,
        modal: true,
        buttons: {
            Cancel: function () {
                dialogAddProduct.dialog("close");
            }
        },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
        },
        close: function () {
        }
    });
    //------------------Grid settings---------------------
    $("#list2").jqGrid({
        url: '/Product/Products',
        datatype: "json",
        mtype: 'get',
        colNames: ['Id', 'Name', 'Price'],
        colModel: [
            { name: 'Id', index: 'Id', width: 30, stype: 'text', key: true, editrules: { number: true } },
            {
                name: 'Name', index: 'Name', width: 150, editable: true, edittype: 'text', sortable: true, editrules: {
                    required: true, custom: true,
                    custom_func: function (value, colname) {
                        if (value.length > valRules.ProductNameValidationRules.maxLenght) {
                            return [false, valRules.ProductNameValidationRules.maxLengthMsg];
                        }
                        var result = null;
                        $.ajax({
                            async: false,
                            url: '/Product/CheckUniqueProductByName?ProductName=' + value,
                            dataType: 'json',
                            contentType: 'application/json; charset=utf-8',
                            success: function (data) {
                                if (data) {
                                    result = [true, ''];
                                } else {
                                    result = [false, 'Name has to be unique'];
                                }
                            },
                            error: function () { }
                        });
                      
                        return result;
                    }
                }
            },
            { name: 'Price', index: 'Price', width: 150, editable: true, edittype: 'text', sortable: true, editrules: { required: true, number: true, maxValue: valRules.ProductPriceValidationRules.maxVal } }
        ],
        onSelectRow: function (id) {
            var row = $(this).getRowData(id);
            dialogManageProduct.data('product', row);
            loadStatisticsSummary(row.Id);
        },
        rowNum: 25,
        rowList: [10, 25, 50, 100],
        pager: '#pager2',
        sortName: 'Name',
        viewrecords: true,
        sortOrder: "desc",
        height: 400,
        autowidth: true,
        editurl: '/Product/EditProduct',
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.root; },
            page: function (obj) { return obj.page; },
            total: function (obj) { return obj.total; },
            records: function (obj) { return obj.records; }
        }
    });

    //-------------handlers---------------------
    $("#AddProduct").click(function (e) {
        e.preventDefault();
        var product = new Object();
        product.Price = $('#NewProductPrice').val();
        product.Name = $('#NewProductName').val();
        if ($('#AddProductForm').valid()) {
            post('/Product/AddProduct',
                      product,
                      function (result) {
                          if (result.result == 'fail') {
                              var summary = "You have the following errors:<br/>";
                              result.errors.forEach(function (err) {
                                  summary += " * " + err + '<br/>';
                              });
                              $('#AddProductSummaryErrors').html(summary);
                              return;
                          }
                          $('#list2').trigger('reloadGrid');
                          $('#NewProductPrice').val('');
                          $('#NewProductName').val('');
                      });
        }
    });
    //------------Product edit------------------
    $("#EditProduct").click(function () {
        if (!$('#dialogManageProduct').data('product')) {
            return;
        }
        editProductId = $('#dialogManageProduct').data('product').Id;
        $("#list2").jqGrid('editRow', editProductId);
        editState();
    });
    $("#SaveProduct").click(function () {
        $("#list2").jqGrid('saveRow', editProductId,
      {
          successfunc: function (response) {
              viewState();
              return true;
          }
      });

    });
    $("#CancelEditProduct").click(function () {

        $("#list2").jqGrid('restoreRow', editProductId);
        viewState();
    });
    $("#DeleteProduct").click(function () {
        if ($('#dialogManageProduct').data('product').Id)
        dialogDelProduct.dialog("open");
    });
    

    $("#ManageProduct").click(function () {
        if ($('#dialogManageProduct').data('product').Id)
        dialogManageProduct.dialog("open");
    });
    $("#AddProductModal").click(function () {
        dialogAddProduct.dialog("open");
    });
    viewState();

    
    function manageProduct() {
        var type = $("select#type").val();
        var statistic = { TypeId: type, Amount: $('#ManageAmount').val(), ProductId: $('#ManageProductId').val() };

        if ($('#ManageProductForm').valid()) {
            $.ajax({
                type: "POST",
                url: '/Statistics/ManageProduct/',
                data: JSON.stringify(statistic),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    loadStatisticsSummary(statistic.ProductId);
                    dialogManageProduct.dialog('close');
                },
                failure: function (errMsg) {
                  
                }
            });
        }
    }

   

    function loadStatisticsSummary(productId) {
        $.get('/Statistics/StatisticsSummary?productId=' + productId, function (data) {
            $('#statsName').text(data.ProductName);
            $('#statsAmount').text(data.Amount);
            $('#statsPrice').text(data.Price);
            $('#statsTotalPrice').text(data.TotalPrice);
        });

    }
   
    function post(url, objToSend, successFunc, failFunc, sync) {
        $.ajax({
            async: !sync,
            type: "POST",
            url: url,
            data: JSON.stringify(objToSend),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: successFunc,
            failure: failFunc
        });
    };

});
