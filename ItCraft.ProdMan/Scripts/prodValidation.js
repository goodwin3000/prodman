﻿$(function() {
    
    var submitted = false;
    //------------validation rules ------------------

    $('#AddProductForm').validate({
        showErrors: function (errorMap, errorList) {
            $('#AddProductSummaryErrors').html("");
            if (submitted) {
               
                var summary = "You have the following errors:<br/>";
                $.each(errorList, function() { summary += " * " + this.message+'<br/>'; });
                $('#AddProductSummaryErrors').html(summary);
                submitted = false;
            }
            this.defaultShowErrors();
        },
        invalidHandler: function(form, validator) {
            submitted = true;
        },
        rules: {
            ProductName: {
                required: true,
                maxlength: valRules.ProductNameValidationRules.maxLenght,
                remote: {
                    url: "/Product/CheckUniqueProductByName",
                    async:false ,
                    type: "get"
                }
            },
            ProductPrice: {
                required: true,
                number: true,
                range: [0, valRules.ProductPriceValidationRules.maxVal]
            }
        }, messages: {
            ProductName: {
                remote: valRules.ProductNameValidationRules.uniqueMsg
            }
        }
    });
    $('#ManageProductForm').validate({
        showErrors: function (errorMap, errorList) {
            $('#ManageProductSummaryErrors').html("");
            if (submitted) {
                var summary = "You have the following errors:<br/>";
                $.each(errorList, function() { summary += " * " + this.message+'<br/>'; });
                $('#ManageProductSummaryErrors').html(summary);
                submitted = false;
            }
            this.defaultShowErrors();
        },          
        invalidHandler: function(form, validator) {
            submitted = true;
        },
        rules: {
            Amount: {
                digits: true,
                required: true,
                range: [0, 1000]
            }
        },
        messages: {
            Amount: {
                digits: "Only positive numbers are allowed"
            }
        }
    });
});
    function ValidationRules() {
        this.ProductNameValidationRules = { maxLenght: 50, maxLengthMsg: 'Max lenght is 50', uniqueMsg: 'Product name has to be unique' };
        this.ProductPriceValidationRules = { maxVal: 10000 };
    }

    var valRules = new ValidationRules();



   
    //------------validation rules end------------------

