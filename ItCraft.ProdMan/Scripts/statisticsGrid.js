﻿$(function () {
    var dialogStatistics;
    dialogStatistics = $("#dialogStatistics").dialog({
        autoOpen: false,
        height: 500,
        width: 950,
        modal: true,
        buttons: {
            Cancel: function () {
                dialogStatistics.dialog("close");
            }
        },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
           
        },
        close: function () {
        }
    });
    $("#StatsProduct").click(function () {
        if (!$('#dialogManageProduct').data('product')) {
            return;
        }
        $("#StatList").jqGrid('setGridParam', { datatype: 'json' });
        $("#StatList").jqGrid('setGridParam', { url: '/Statistics/Statistics?ProductId=' + $('#dialogManageProduct').data('product').Id }).trigger('reloadGrid');
        dialogStatistics.dialog("open");
    });
    //------------------Grid settings---------------------
    $("#StatList").jqGrid({
       
        datatype: 'local',
        mtype: 'get',
        colNames: ['User name', 'Operation type', 'Amount','Date'],
        colModel: [
            
            { name: 'UserName', index: 'UserName', sortable: true },
            { name: 'OperationTypeName', index: 'OperationTypeName', stype: 'text', sortable: true },
            { name: 'Amount', index: 'Amount', sortable: true },
             { name: 'Date', index: 'Date', sortable: true,formatter: "date", formatoptions: { srcformat: "ISO8601Long", newformat: "m/d/Y h:i A" } }
        ],
        rowNum: 10,
        rowList: [10, 25, 50, 100],
        pager: '#StatListPager',
        sortName: 'Name',
        viewrecords: true,
        sortOrder: "desc",
        height: 200,
        width:900,
       
        jsonReader: {
            repeatitems: false,
            root: function (obj) { return obj.root; },
            page: function (obj) { return obj.page; },
            total: function (obj) { return obj.total; },
            records: function (obj) { return obj.records; }
        }
    });

});
