﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using AutoMapper;
using ItCraft.ProdMan.Application.Dto;
using ItCraft.ProdMan.Domain;
using ItCraft.ProdMan.Infrastracture.Enums;
using Microsoft.AspNet.Identity;
using Ninject.Activation;

namespace ItCraft.ProdMan.MappConfig
{
    public class MappConfig
    {
        public static void Mapping()
        {
            Mapper.CreateMap<Product, ProductDto>();
            Mapper.CreateMap<ProductDto, Product>();
            Mapper.CreateMap<Statistic, StatisticDto>().ForMember(
                    dest => dest.UserName, opt => opt.MapFrom(src => src.User.UserName)
                )
                 .ForMember(
                    dest => dest.OperationTypeName, opt => opt.MapFrom(x => x.Operation.Type)
                )
                 .AfterMap((src, dest) =>
                 {
                         dest.Amount = System.Math.Abs(dest.Amount);
                 });

            Mapper.CreateMap<StatisticDto, Statistic>()
                .ForMember(
                    dest => dest.OperationId,opt=>opt.MapFrom(src=>src.TypeId) 
                )
                .ForMember(
                    dest => dest.Date, opt => opt.MapFrom(x => DateTime.Now)
                )
                .AfterMap((src, dest) =>
                 {
                     if (src.TypeId ==(int) OperationTypes.Remove)
                     {
                         dest.Amount = 0 - dest.Amount;
                     }
                 });

        }


    }
}