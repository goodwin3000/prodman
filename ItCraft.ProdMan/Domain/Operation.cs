﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ItCraft.ProdMan.Domain
{
    public class Operation:BaseEntity
    {
        public string Type { get; set; }
        public virtual ICollection<Statistic> Statistics{ get; set; }
    }
}