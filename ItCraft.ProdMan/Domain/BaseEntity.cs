﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ItCraft.ProdMan.Domain
{
    public class BaseEntity
    {
        public virtual int Id { get; set; }
    }
}