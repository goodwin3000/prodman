﻿using System;

namespace ItCraft.ProdMan.Domain
{
    public class Statistic: BaseEntity
    {
        
        public int Amount { get; set; }

       
        public DateTime Date { get; set; }
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
        public string UserId { get; set; }
        public int OperationId { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual  Operation   Operation { get; set; }
    }
}