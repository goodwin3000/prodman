﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItCraft.ProdMan.Domain
{
    public class Product: BaseEntity
    {[Required]
        public string Name { get; set; }
        [Required]
        public decimal Price { get; set; }
        public virtual ICollection<Statistic> Statistic { get; set; }
    }
}