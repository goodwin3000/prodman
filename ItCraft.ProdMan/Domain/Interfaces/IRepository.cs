﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ItCraft.ProdMan.Domain.Interfaces
{
    public interface IRepository<T> where T : class
    {
        void Add(T obj);
        T GetById(int id);
        IQueryable<T> GetAll();
        IQueryable<T> GetAll(Expression<Func<T, bool>> predicate);
        void Update(T obj);
        void Remove(T obj);
        void AddOrUpdate(T obj);
        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);
        void Save();
        int ExecuteQuery(string query);
    }
}